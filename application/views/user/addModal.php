<div class="modal fade" id="userModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">User Profile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="alert alert-danger form_errors" style="display: none">
         
          
        </div>
        <form id="userForm">
          <input type="hidden" name="id">
        <div class="row form-group">
          <div class="col-md-3">
            <label for="first_name">First Name</label>
            
          </div>
          <div class="col-md-9">
            <input type="text" name="first_name" class="form-control">
            
          </div>

        </div>

        

         <div class="row form-group">
          <div class="col-md-3">
            <label for="last_name">Last Name</label>
            
          </div>
          <div class="col-md-9">
            <input type="text" name="last_name" class="form-control">
            
          </div>
        </div>

         <div class="row form-group">
          <div class="col-md-3">
            <label for="phone">Phone </label>
            
          </div>
          <div class="col-md-9">
            <input type="number" name="phone" class="form-control">
            
          </div>
        </div>


         <div class="row form-group">
          <div class="col-md-3">
            <label for="email">Email </label>
            
          </div>
          <div class="col-md-9">
            <input type="email" name="email" class="form-control">
            
          </div>
        </div>
        <div class="row">
           <div class="col-md-12">
           <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary pull-right">Save changes</button>
          </div>
        </div>
      </form>
      </div>
      
    </div>
  </div>
</div>