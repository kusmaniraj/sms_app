<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {
	private $data;

	public function __construct()
	{
		parent::__construct();
		$this->load->model('AppModel');
		$this->load->library('form_validation');
		
		
	}
	public function index(){
		
		
	}
	public function storeUser($id=NULL){
		$config=[
			[ 
				'field' => 'first_name',
                'label' => 'first_name',
                'rules' => 'required'
            ],
            [ 
				'field' => 'last_name',
                'label' => 'last_name',
                'rules' => 'required'
            ],
            [ 
				'field' => 'phone',
                'label' => 'phone',
                'rules' => 'required|numeric|max_length[10]|min_length[10]|is_unique[tbl_user.phone.id.'.$id.']'
            ],
            [ 
				'field' => 'email',
                'label' => 'email',
                'rules' => 'required|valid_email|is_unique[tbl_user.email.id.'.$id.']'
            ]
		];
		if($id){

		}

		
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() ==FALSE){
			 echo json_encode(['validation_error'=>validation_errors()]);
			 exit();

		}

		$formData=$this->input->post();

		if($id){
			$result=$this->AppModel->update('tbl_user',$formData,['id'=>$id]);
			if ($result) {
				 echo json_encode(['success'=>'successfully Updated']);
			}else{
			echo json_encode(['error'=>'Cannot Updated']);
			}

		}else{
			$result=$this->AppModel->insert('tbl_user',$formData);
			if ($result) {
				 echo json_encode(['success'=>'successfully Added']);
			}else{
			echo json_encode(['error'=>'Cannot Added']);
			}

		}
		
	}
	function edit($id){
			echo json_encode($this->AppModel->fetch('*','tbl_user',['id'=>$id])) ;
	}
	function delete($id){
		$result=$this->AppModel->delete('tbl_user',['id'=>$id]);
		if ($result) {
			 echo json_encode(['success'=>'successfully Remove User Contact']);
		}else{
		echo json_encode(['error'=>'Cannot Remove User Contact']);
		}
	}

	//functions
	}