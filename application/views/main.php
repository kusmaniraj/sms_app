<?php  $this->load->view('includes/header')?>
<link rel="stylesheet" href="<?=base_url('public/plugins/multiselect/multiselect.css')?>" type="text/css"/>
<link rel="stylesheet" href="<?=base_url('public/css/main.css')?>" type="text/css"/>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css">

	<div class="row">
		<div class="container ">
			<div class="col-md-offset-2 col-md-8 well ">
				 <div class="loader " style="display: none" id="ajax-loader"></div>
				  
				        <h2 class="text-center">SMS APP</h2>  
				         
				       <!--  <div class=" col-md-12 form-group">
				            <input type="text" class="form-control" placeholder="API key" name="key" ">
				        </div>
				         <div class=" col-md-12 form-group">
				            <input type="text" class="form-control" name="sender_name" placeholder="Sender Name">
				        </div> -->
				        <div class="col-md-12 form-group  navbar">
				        	<div class="col-md-2">
				        		<div class="dropdown">
 									 <button class="btn btn-default btn-sm dropdown-toggle" type="button" data-toggle="dropdown"><i class="fa fa-address-book"></i>&nbsp;Users 
										  <span class="caret"></span></button>
										  <ul class="dropdown-menu">
										    <li><a href="#" class="user_add_btn"><i class="fa fa-plus"></i>&nbsp;Add User</a></li>
										    <li><a href="#" class="user_list_btn"><i class="fa fa-users"></i>&nbsp;List User</a></li>
										  </ul>
								</div>
														        		
				        	</div>
				        	<div class="col-md-3 log">
				        		 <button class="btn btn-default btn-sm deliveryReportBtn" type="button" ><i class="fa fa-mobile"></i>&nbsp;Delivery Report</button>
				        		
				        	</div>
				        	<div class=" col-md-6">
				        		
				        			<input type="file" id="fileLoader" name="contact_file" title="Load Contact" style="display: none"  accept=".csv"/>
									<button type="button" class="btn btn-sm  btn-default" id="btnOpenFileDialog"> <i class="fa fa-file">	</i>&nbsp;Import CSV</button>
									<span style="" id="filePreview"></span>
									<button type="button" class="btn btn-success btn-sm pull-right" id="saveFile" style="display: none"><i class="fa fa-save">	</i></button>
				        	
				        		

				        	</div>
				        	
				        </div>

				        <!-- send SMS div -->
				        <div class="col-md-12 form-group sendSms well">
				        	 
				        	<div class="header text-center ">
				        		<h4>Send SMS</h4>
				        		
				        	</div>
				        	  <form id="sendSmsForm" method="post" enctype="multipart/form-data">
				        	  	   <div class="alert alert-danger form_errors" style="display: none"></div>  
						        	  	 <div class="col-md-12 form-group">
						        	
						        		<select class="form-control" id="multiselect" name="user_id[]" multiple="multiple">
											        
		   							 	</select>
		   							 	</div>


					   				<div class="col-md-12 form-group">
									           <textarea rows="4" class="form-control" name="message" placeholder="Enter Message"></textarea>
									        </div>
									        <div class=" col-md-12 form-group">
									            <button class="btn btn-primary form-control" type="submit" >Send Message</button>
									        </div>
				        		  </form>
				        	
				        	

				        	 
				        </div>

				        <!-- user lists Page -->
				        <?php  $this->load->view('user/userList')?>
				         <!-- Delivery Report Page -->
				        <?php  $this->load->view('deliveryReport/list')?>
				        
				        
				            
				  
    
			</div>
		</div>
		
	</div>
	<!-- load User Modal -->

<?php  $this->load->view('user/addModal')?>
<script type="text/javascript" src="<?=base_url('public/plugins/multiselect/multiselect.js')?>"></script>
<script type="text/javascript" src="<?=base_url('public/js/main.js')?>"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>



		
	
<?php  $this->load->view('includes/footer')?>




