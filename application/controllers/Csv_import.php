<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Csv_import extends CI_Controller {
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('AppModel');
		$this->load->library('csvimport');
	}
	function getContactFile(){
		$users_contact=$this->AppModel->fetch_all('*','tbl_user');
		echo json_encode($users_contact);

	}

	function import()
	{
		$result=false;


		
		if(empty($_FILES["contact_file"]["tmp_name"])){
			
		echo json_encode(['error'=>'Please Select CSV File']);
		exit();
	

		}

		if(isset($_FILES["contact_file"]["tmp_name"])){
			$file_data = $this->csvimport->get_array($_FILES["contact_file"]["tmp_name"]);


		foreach($file_data as $row)
		{
			if(isset($row["First Name"])==false && isset($row["Last Name"])==false && isset($row["Phone"])==false && isset($row["Email"])==false){
				echo json_encode(['error'=>'Selected CSV unknown colunm List']);
			exit();
			}


			$data = array(
				'first_name'	=>	$row["First Name"],
        		'last_name'		=>	$row["Last Name"],
        		'phone'			=>	$row["Phone"],
        		'email'			=>	$row["Email"]
			);

			$old_file_users=$this->AppModel->fetch_all('*','tbl_user');
			foreach ($old_file_users as $key => $user) {
				if($user['phone']===$row["Phone"]){
						$this->AppModel->delete('tbl_user',['phone'=>$row["Phone"]]);

				}
			}

			$this->AppModel->insert('tbl_user',$data);



			
			$result=true;
		}

		if ($result) {
			 echo json_encode(['success'=>'successfully Added']);
		}else{
				 echo json_encode(['success'=>'Cannot Added']);
		}


	}
		
		
	}
	
	
		
}
