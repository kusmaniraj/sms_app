var main = new function () {
	

	var $self = this;
	var multiselect;
	var userListTable;
	var deliveryTable;
	


	this.bindEvent = function () {

		// get contacts
		$self.addUserContacts();
	

		// Multisselect 
		
        //open Dialog file using BTN
        $('#btnOpenFileDialog').on('click',function(){
        	$self.openfileDialog();
        }) 

		$("#fileLoader").on('change',function(){
        		var filename = $(this).val().replace(/C:\\fakepath\\/i, '');
        		$('#filePreview').html('<label class="label label-success">'+filename+'</label>');
        		$('#saveFile').show();

        		
        })
        $('#saveFile').on('click',function(e){
        	e.preventDefault();
        	$self.importCSV();
        	 

        })


        // add user
        $('.user_add_btn').on('click',function(){
        	$('#userForm input ').val('');
        	$self._hide_display_errors('userModal');
        	$('#userModal').modal('show');
        	$('#userForm button[type="submit"]').text('Add');
        })

         $('.userListTable ').on('click','.editUserBtn',function(){
         	$self._hide_display_errors('userModal');
        	var id=$(this).data('id');
        	$self.editUser(id);
        
        })

        $('.userListTable ').on('click','.delUserBtn',function(){
        	var id=$(this).data('id');
        	if(confirm('Are you Sure Want to Remove ?')==false){
        		return false;
        	}else{
        			$self.deleteUser(id);
        	}
        
        })

        // list user
        $('.user_list_btn').on('click',function(){
        	// load datatable
        	$self.declareUserTable();
        	$self.showUserList();
        })

         // list user
        $('.deliveryReportBtn').on('click',function(){
        	// load datatable
        	$self.declareDeliveryReportTable();
        	$self.showdeliveryReport();
        })
         // Back from User list
        $('.users .backBtn').on('click',function(){
        	// 
        	$self.showMainPage('.users');
        })

         // Back from Delivery Report 
        $('.deliveryReport .backBtn').on('click',function(){
        	// 
        	$self.showMainPage('.deliveryReport');
        })

        // store user
        $('#userForm').on('submit',function(e){
        		e.preventDefault();
        	var formData=$(this).serialize();
        	var id=$('#userForm input[name="id"]').val();
        	if(id){
        		$self.updateUser(formData,id);
        	}else{
        		$self.storeUser(formData);
        	}
        	
        })
		// send SMS
		// store user
        $('#sendSmsForm').on('submit',function(e){
        		e.preventDefault();
        	var formData=$(this).serialize();
        	$self._hide_display_errors('sendSmsForm');
        	$self.sendSms(formData);
        })

	}

	// End Bind function

	// start declare function
	this.declareUserTable=function(selector){
		userListTable=$('.userListTable').DataTable();
	}

	this.declareDeliveryReportTable=function(selector){
		deliveryTable=$('.deliveryReportTable').DataTable();
	}

	// multisekect
	this.multiselectFunction=function(){
	multiselect=$('#multiselect').multiselect({
        	 buttonWidth: '100%',
        	 nonSelectedText: 'Select Receiver Users',
			  enableFiltering: true,
			  enableCaseInsensitiveFiltering: true,
        });
	}

	// add Contact
	this.addUserContacts=function(){
		
		$.get(baseUrl+'csv_import/getContactFile',function(response){
			$('#multiselect').multiselect('destroy');
			var data=JSON.parse(response);
			var contact_rows='';
			$.each(data,function(i,v){
				contact_rows +='<option value="'+v.id+'" >'+v.first_name+'&nbsp; '+v.last_name+'('+v.phone+')</option>';

			})
		
			$(document).find('Select#multiselect').html(contact_rows);
			$self.multiselectFunction();
		})

			
		
			

	}
	this.storeUser=function(formData){
		$.ajax({
			url:baseUrl+'users/storeUser',
			method:"POST",
			data:formData,
			success:function(response)
			{
				
			var data=JSON.parse(response);
				
				if(data.success){
					alertify.success(data.success);
					$self.addUserContacts();
					$('#userModal').modal('hide');
					$self.usersList();


				}else if(data.error){
					alertify.error(data.error);
				}else{
					$self.formValidations(data.validation_error,'userModal');
				}
				
				
			}
		})

	}
	this.updateUser=function(formData,id){
		$.ajax({
			url:baseUrl+'users/storeUser/'+id,
			method:"POST",
			data:formData,
			success:function(response)
			{
				
			var data=JSON.parse(response);
				
				if(data.success){
					alertify.success(data.success);
					$self.addUserContacts();
					$('#userModal').modal('hide');
					$self.usersList();


				}else if(data.error){
					alertify.error(data.error);
				}else{
					$self.formValidations(data.validation_error,'userModal');
				}
				
				
			}
		})

	}
	this.editUser=function(id){
		$.get(baseUrl+'users/edit/'+id,function(response){
			var data=JSON.parse(response);
			$('#userModal').modal('show');
			$('#userForm input[name="id"]').val(data.id);
			$('#userForm input[name="first_name"]').val(data.first_name);
			$('#userForm input[name="last_name"]').val(data.last_name);
			$('#userForm input[name="phone"]').val(data.phone);
			$('#userForm input[name="email"]').val(data.email);
			$('#userForm button[type="submit"]').text('Update');
			

		})

	}
	this.deleteUser=function(id){
		$.get(baseUrl+'users/delete/'+id,function(response){
			var data=JSON.parse(response);
			if(data.success){
					alertify.success(data.success);
					$self.usersList();


				}else {
					alertify.error(data.error);
				}

		})
	}

	this.sendSms=function(formData){
		 $('#ajax-loader').show();
		$.ajax({
			url:baseUrl+'main/sendSms',
			method:"POST",
			data:formData,
			success:function(response)
			{
				
			var data=JSON.parse(response);
				
				if(data.success){
					alertify.success(data.success);
					$('#sendSmsForm input').val('');
					$self.addUserContacts();
					
					

				}else if(data.error){
					alertify.error(data.error);

				
				
				}else{
					$self.formValidations(data.validation_error,'sendSmsForm');
				}

				 $('#ajax-loader').hide();
				
				
			}
		})

	}


	this.importCSV=function(){
		var formData = new FormData();
       			 formData.append('contact_file', $('input[name="contact_file"]') [0].files[0]);
       			  $('#ajax-loader').show();
        	
        	
			
        	$.ajax({
			url:baseUrl+'csv_import/import',
			method:"POST",
			data:formData,
			contentType:false,
			
			processData:false,
			
			success:function(response)
			{
				
			
				var data=JSON.parse(response);
				
				if(data.success){
					alertify.success(data.success);
					$self.addUserContacts();
					$('input[name="contact_file"]').val('');
					$('#filePreview').html('');
					$('#saveFile').hide();


				}else{
					alertify.error(data.error);
				}

				
			}
		})
        	 $('#ajax-loader').hide();
	}
	this.openfileDialog=function(){
        	$("#fileLoader").click();
        	
        }

    

this.showUserList = function () {

		$('.sendSms').hide('slide', {direction: 'left'}, 500);
		window.setTimeout(function () {
			$('.users').show('slide', {direction: 'left'}, 500);
			$self.usersList();

		}, 300);
}

this.showdeliveryReport = function () {

		$('.sendSms').hide('slide', {direction: 'left'}, 500);
		window.setTimeout(function () {
			$('.deliveryReport').show('slide', {direction: 'left'}, 500);
			$self.deliveryReportList();

		}, 300);
}

	this.showMainPage = function (hideSelector) {

		$(hideSelector).hide('slide', {direction: 'left'}, 500);
		window.setTimeout(function () {
			$('.sendSms').show('slide', {direction: 'left'}, 500);
			$self.usersList();

		}, 300);
	}
	this.deliveryReportList=function(){
		$.get(baseUrl+'main/getDeliveryReport',function(response){
			deliveryTable.destroy();
			var data=JSON.parse(response);
			var row='';
			$.each(data,function(i,v){
				row  +='<tr>'+
				'<td>'+(i+1)+'</td>'+
				'<td>'+v.MSISDN+'</td>'+
				'<td>'+v.delivery_time+'</td>'+
				'<td>'+v.delivery_value+'</td>'+
				'<td>'+v.message+'</td>'+
				'<td> <a href="#" class="btn btn-danger delUserBtn"><i class="fa fa-trash"></i></a> </td>'+
				'</tr>';

			})
			$('.deliveryReport .deliveryReportTable tbody').html(row);
			// load datatable
        	$self.declareDeliveryReportTable();

		})
	}

	this.usersList=function(){
		$.get(baseUrl+'main/getUserList',function(response){
			if(userListTable){
					userListTable.destroy();
			}
		
			var data=JSON.parse(response);
			var row='';
			$.each(data,function(i,v){
				row  +='<tr>'+
				'<td>'+(i+1)+'</td>'+
				'<td>'+v.first_name+'&nbsp;'+v.last_name+'</td>'+
				'<td>'+v.phone+'</td>'+
				'<td> <a href="#" data-id="'+v.id+'" class="btn btn-danger btn-sm delUserBtn"><i class="fa fa-trash"></i></a>'+ 
				'&nbsp; <a href="#" data-id="'+v.id+'" class="btn btn-info btn-sm editUserBtn"><i class="fa fa-edit"></i></a> </td>'+
				'</tr>';

			})
			$('.users .userListTable tbody').html(row);
			// load datatable
        	$self.declareUserTable();

		})
	}

	this.formValidations=function(data,selectorID){
	$('#'+selectorID+  ' .form_errors').show().html(data)
	
}

 this._hide_display_errors=function(selectorID){

	
	$('#'+selectorID+  ' .form_errors').hide().html();
	
}


	


	var init = function () {


		$self.bindEvent();
		
		console.log("Initializing");
	}();
}();
