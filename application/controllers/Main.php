<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Main extends CI_Controller {
	private $data;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('AppModel');
		$this->load->library('form_validation');
		
	}
	public function index(){
		
		$this->load->view('main',$this->data);
	}
	public function sendSms(){


		$config=[
			// [ 
			// 	'field' => 'key',
   //              'label' => 'key',
   //              'rules' => 'required'
   //          ],
    //        [ 
				// 'field' => 'sender_name',
    //             'label' => 'sender_name',
    //             'rules' => 'required'
    //         ],
           
            [ 
				'field' => 'user_id[]',
                'label' => 'user_id',
                'errors' => [
	            				'required' =>'Please Select Receiver User',
	        				],
                'rules' => 'required'
            ],
             [ 
				'field' => 'message',
                'label' => 'message',
                'errors' => [
	            				'required' =>'Please Input Message ',
	        				],
                'rules' => 'required'
            ],
		];
		$this->form_validation->set_rules($config);
		if($this->form_validation->run() ==FALSE){
			 echo json_encode(['validation_error'=>validation_errors()]);
			 exit();

		}
		$formData=$this->input->post();
		$users=$this->AppModel->fetch_all('*','tbl_user');
		$receivers_phone_no=[];
		$users_id=$formData['user_id'];
		for ($i=0; $i <count($users_id) ; $i++) { 
			foreach ($users as $key => $user) {
			if($user['id']==$users_id[$i]){
				
				array_push($receivers_phone_no, $user['phone']);
			}
		}
			
		}
		
		

		
// Account details
	$apiKey = 'iDbd2gN2sa8-iQmoaKf9UCjPxK9yBn5ZLH7ZhEjwqD';
	
	// Message details
	
	$sender = urlencode('Kusma');
	$message = rawurlencode($formData['message']);
 
	$numbers =  implode(',',$receivers_phone_no);
 
	// Prepare data for POST request
	$data = array('apikey' => $apiKey, 'numbers' => $numbers, "sender" => $sender, "message" => $message);
 
	// Send the POST request with cURL
	$ch = curl_init('https://api.txtlocal.com/send/');
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$response= curl_exec($ch);
	$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

	curl_close($ch);
	$output=json_decode($response, true);
	if($output['status']=='success'){
			echo json_encode(['success'=>' send Message']);
	}else{
		$errorMsg='<strong>Status:</strong>:'.$output['status'];
		foreach ($output['errors'] as  $error) {
		$errorMsg .= '<strong>code:</strong>'.$error['code'].' <strong>message:</strong>:'.$error['message'];

			
		}
	
		echo json_encode(['error'=>'Cannot send Message,'.$errorMsg]);
		
	}
	

	
	
   
	}
	public function getUserList(){
			$users=$this->AppModel->fetch_all('*','tbl_user');
			echo json_encode($users);
	}
	public function getDeliveryReport(){
			$delivery_reports=$this->AppModel->fetch_all('*','delivery_report');
			echo json_encode($delivery_reports);
	}

	
	}