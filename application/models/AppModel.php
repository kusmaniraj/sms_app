<?php


class AppModel extends CI_Model
{
   
    public function fetch($select,$table,$colValsArray,$orderBy=NULL)
    {
        if ($select) {
            $this->db->select($select);

        }
        $this->db->where($colValsArray);
		if ($orderBy) {

			foreach ($orderBy as $ordercol => $orderval) {
				$this->db->order_by($ordercol, $orderval);
			}

		}

        $result = $this->db->get($table);
        return $result->row_array();


    }

    /**
     * @param null $colValsArray
     * @param $select
   
     * $orderArray in format array('colname1'=>'val1')
     * @param $table
     * @param null $limit
     * @param null $orderBY
     * @return mixed
     */


    public function fetch_all($select, $table,$colValsArray = NULL,  $limit = NULL, $searchFor=NULL, $orderBy=NULL,$groupBy=NULL)
    {
        if ($select) {
            $this->db->select($select);
        }

        if ($limit) {

            $this->db->limit($limit);
        }
        if ($colValsArray) {
           
            foreach ($colValsArray as $wherecol => $whereval) {
            $this->db->where($wherecol, $whereval);
             }
        }


        if ($orderBy) {

            foreach ($orderBy as $ordercol => $orderval) {
                            $this->db->order_by($ordercol, $orderval);
                        }
                 
          }
          if ($groupBy) {
                $this->db->group_by($groupBy);
                 
          }


        $result = $this->db->get($table);
        return $result->result_array();
    }

    /**
     * @param $data : data to add in the table
     * @param $table : name of table to add data
     * @return returns id of the new resource added
     *get $colValsArray in arra('fiedl_name'=>$values)
     */
    public
    function insert($table,$data, $colValsArray = NULL)
    {
        if ($colValsArray) {


            $this->db->where($colValsArray);


        }
        $data['c_date'] = date('Y-m-d-h-m-s');
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    /**
     * @param $col : name of the column to identify
     * @param $val : value of the column to identify
     * @param $data : what data to update in table
     * @param $table : name of the table to update data
     *get $colValsArray in arra('fiedl_name'=>$values)
     * @return true or false according the the sucess and failure of query
     */
    public
    function update( $table,$data,$colValsArray)
    {

        if ($colValsArray) {


            $this->db->where($colValsArray);


        }
        $data['u_date'] =  date('Y-m-d-h-m-s');
        return $this->db->update($table, $data);

    }

    /**
     * @param $col : name of the column to identify
     * @param $val : value of column to identify
     * @param $table : name of the table to delete data
     *get $colValsArray in arra('fiedl_name'=>$values)
     */
    public
    function delete($table,$colValsArray)
    {
       
            $this->db->where($colValsArray);
          return $this->db->delete($table);
    }


    /**
     * @param $col : name of the column to identify
     * @param $val : value of column to identify
     * @param $table : name of the table to delete data
     *
     * * get $colArray in this format array('col_name','colname2')
     * get $valArray in this format array('valu1','value2')
     * @return total count of rows
     */
    public
    function count_rows( $table,$colValsArray = NULL)
    {

        if ($colValsArray) {
            $this->db->where($colValsArray);
            }
        $result = $this->db->get($table);
        return $result->num_rows();
    }
    public
    function plain_join($select,$tablesJoin, $columnsToJoin,$whereConditions="", $limit = "",$orderConditions="",$order_by="")
    {
// eg plain_join('*',['product_information','orders'],'product_information.id=orders.product_id');

        $this->db->select($select);
        $this->db->where($columnsToJoin);
        if($whereConditions){
             $this->db->where($whereConditions);
        }
       
         if ($limit) {
            $this->db->limit($limit);
        }

         if ($orderConditions) {
           
            $this->db->order_by($order_by);
                 
          }


        $query = $this->db->get($tablesJoin);
          return $query->result_array();


    }

}
